//
//  ViewController.swift
//  KeyDetectionDemo
//
//  Created by Alexander Momchilov on 2021-04-05.
//

import Cocoa
import Carbon.HIToolbox

class ViewController: NSViewController {

	@IBOutlet var label: NSTextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()

		NSEvent.addLocalMonitorForEvents(matching: .keyDown) { self.keyDown(with: $0); return nil }
		NSEvent.addLocalMonitorForEvents(matching: .keyUp) { self.keyDown(with: $0); return nil }
		NSEvent.addLocalMonitorForEvents(matching: .flagsChanged) {
			print($0)
			self.label.stringValue = "\(self.describeModifierFlags($0.modifierFlags))"
			return nil
		}
	}

	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}
	
	override var acceptsFirstResponder: Bool { true }

	override func becomeFirstResponder() -> Bool {
		true
	}
	

	override func keyDown(with event: NSEvent) {
		switch event.type {
		case .keyDown:
			self.label.stringValue = "Key down \(describeKeyEvent(event))"
			
			
		case .keyUp:
			self.label.stringValue = "Key up \(describeKeyEvent(event))"
			
		default: print("Ignored event type: \(event.type)")
		}
	}
	
	func describeKeyEvent(_ event: NSEvent) -> String {
		assert([.keyDown, .keyUp].contains(event.type))
		
		let keyDescription: String
		if let specialKey = event.specialKey {
			print(event)
			keyDescription = String(specialKey.unicodeScalar)
		} else {
			keyDescription = String(event.characters!)
		}
		
		return "\(describeModifierFlags(event.modifierFlags)) \(keyDescription)"
	}
	
	let ignoredFlagsMask = [
		activeFlag,
		mDownMask,
		mUpMask,
		keyDownMask,
		keyUpMask,
		autoKeyMask,
		updateMask,
		diskMask,
		activMask,
		highLevelEventMask,
		osMask,
	].reduce(0, |)
	
	func describeModifierFlags(_ modifiers: NSEvent.ModifierFlags) -> String {
		let originalModifiers = modifiers
		var modifiers = originalModifiers
		var desc = ""
		
		modifiers.subtract(NSEvent.ModifierFlags(rawValue: UInt(ignoredFlagsMask)))
		
		if modifiers.remove(.capsLock) != nil { desc.append("⇪") }
		if modifiers.remove(.function) != nil { desc.append("Fn") }
		if modifiers.remove(.control) != nil { desc.append("⌃") }
		if modifiers.remove(.option) != nil { desc.append("⌥") }
		if modifiers.remove(.shift) != nil { desc.append("⇧") }
		if modifiers.remove(.command) != nil { desc.append("⌘") }
		
		if modifiers.rawValue != 0 {
			let binaryFlags = String(modifiers.rawValue, radix: 2)
			desc += "<Unknown Remaining Modifiers: \(binaryFlags)>"
		}
		
		return desc
	}
}

